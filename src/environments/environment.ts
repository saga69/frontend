// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost:3000/',
  javaWebtoken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtb2RlIjoicncifQ.S1npSh_T-E8Id3b7cadyz5cHX0eaNjMeuMwJRxjletU',
  webSocketUrl: 'ws://localhost:3001',
  firebaseConfig: {
    apiKey: 'AIzaSyB1L7zE01NgsqccxeQKk5fH-Ny4k9fNW0g',
    authDomain: 'bitsaga-97613.firebaseapp.com',
    databaseURL: 'https://bitsaga-97613.firebaseio.com',
    projectId: 'bitsaga-97613',
    storageBucket: 'bitsaga-97613.appspot.com',
    messagingSenderId: '968151154298',
    appId: '1:968151154298:web:7d53ebc0112305227d85c3',
    measurementId: 'G-HB8TM5VR5W'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
