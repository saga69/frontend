export const environment = {
  production: true,
  apiUrl: 'https://api.bitsaga.ca/',
  javaWebtoken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtb2RlIjoicncifQ.S1npSh_T-E8Id3b7cadyz5cHX0eaNjMeuMwJRxjletU',
  webSocketUrl: 'wss://api.bitsaga.ca:8443',
  firebaseConfig: {
    apiKey: 'AIzaSyB1L7zE01NgsqccxeQKk5fH-Ny4k9fNW0g',
    authDomain: 'bitsaga-97613.firebaseapp.com',
    databaseURL: 'https://bitsaga-97613.firebaseio.com',
    projectId: 'bitsaga-97613',
    storageBucket: 'bitsaga-97613.appspot.com',
    messagingSenderId: '968151154298',
    appId: '1:968151154298:web:7d53ebc0112305227d85c3',
    measurementId: 'G-HB8TM5VR5W'
  }
};
