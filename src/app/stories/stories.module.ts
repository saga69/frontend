import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../modules/material.module';
import { BootstrapModule } from '../modules/bootstrap.module';
import { MatListModule } from '@angular/material/list';
import { StoriesComponent } from './stories.component';
import { StoryComponent } from './story/story.component';
import { ContributionComponent } from './story/contribution/contribution.component';
import { AddContributionComponent } from './story/add-contribution/add-contribution.component';
import { AddStoryComponent } from './story/add-story/add-story.component';
import { AddBranchDialogComponent } from './story/add-branch-dialog/add-branch-dialog.component';
import { ViewBranchesDialogComponent } from './story/view-branches-dialog/view-branches-dialog.component';


@NgModule({
  declarations: [
    StoriesComponent,
    StoryComponent,
    ContributionComponent,
    AddContributionComponent,
    AddStoryComponent,
    AddBranchDialogComponent,
    ViewBranchesDialogComponent,
    AddBranchDialogComponent
  ],
  imports: [
    CommonModule,
    BootstrapModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    MatListModule
  ],
  exports: [
    StoryComponent,
  ],
  entryComponents: [AddStoryComponent, ViewBranchesDialogComponent, AddBranchDialogComponent]

})
export class StoriesModule { }
