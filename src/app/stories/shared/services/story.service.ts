import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Story } from '../models/story.model';
import { Contribution } from '../models/contribution.model';
import { Saga } from '../models/saga.model';
import { Branch } from '../models/branch.model';
import { environment } from '../../../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { ErrorHandler } from '../../../config/error-handler';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class StoryService {

  storyChange: EventEmitter<number> = new EventEmitter();

  private BRANCHES = 'branches';
  private CONTRIBUTIONS = 'contributions';
  private SAGAS = 'sagas';
  private ID = 'id';
  private SAGA_ID = 'saga_id';
  private EQ_PREFIX = 'eq.';
  private user: firebase.User;

  private token: string;

  constructor(
    private http: HttpClient,
    private errorHandler: ErrorHandler,
    private afAuth: AngularFireAuth
    ) {
      this.afAuth.user.subscribe(user => {
        if (user) {
          this.user = user;

          this.afAuth.auth.currentUser.getIdToken(false).then(tkn => {
            this.token = tkn;
            console.log(this.token);
          });
        } else {
          this.user = null;
        }
      });

    }

  /**
   * @method GET
   * @returns Observable array of existing sagas
   */
  getSagas(): Observable<Saga[]> {
    const headers = this.buildHeader();
    console.log(headers);
    return this.http
      .get<Story[]>(
        environment.apiUrl + this.SAGAS, { headers }
      ).pipe(
        map((data: any[]) => data.map((item: any) => new Saga(
          item.id,
          item.title,
          new Date(item.created_at),
        ))),
        catchError(this.errorHandler.handleError));
  }

  /**
   * @method GET
   * @param contributionId ID of the contribution
   * @returns An observable Contribution object
   */
  getContribution(contributionId: string): Observable<any> {
    const headers = this.buildHeader();
    console.log(environment.apiUrl + 'contributionz/' + contributionId);
    return this.http
      .get<any>(
        environment.apiUrl + 'contributionz/' + contributionId, {
          headers
        })
      .pipe(
        map((data: any) => {
          return new Contribution(
            data.id,
            data.content,
            data.saga_id,
            data.parent_id,
            new Date(data.created_at)
          );
        }),
        catchError(this.errorHandler.handleError));
  }

 /**
  * @method POST
  * @param sagaId ID of the saga/story contributing to
  * @param parentId ID of the parent contribution
  * @param content The new contribution content to add
  * @returns Observable of the HTTP response
  */
  addContribution(sagaId: number, parentId: number, content: string): Observable<HttpResponse<Contribution>> {
    console.log('saga_id: ' + sagaId + ' parent_id: ' + parentId);
    const headers = this.buildHeader();
    console.log(headers);
    return this.http
      .post<Contribution>(
        environment.apiUrl + this.CONTRIBUTIONS, {
          saga_id: sagaId,
          parent_id: parentId,
          content,
        }, { headers, observe: 'response'})
        .pipe(catchError(this.errorHandler.handleError));
  }

  /**
   * @method GET
   * @param sagaId ID of the saga/story
   * @returns Observable array of contributions for a story
   */
  getStoryContributions(sagaId: string): Observable<Contribution[]> {
    const headers = this.buildHeader();
    return this.http
      .get<Contribution[]>(
        environment.apiUrl + this.CONTRIBUTIONS + '/' + sagaId, {
          headers
        }
      ).pipe(
        map((data: any[]) => data.map((item: any) => new Contribution(
          item.id,
          item.content,
          item.saga_id,
          item.parent_id,
          new Date(item.created_at),
        ))),
        catchError(this.errorHandler.handleError));
  }

  /**
   * @method POST
   * @param title saga's title
   * @returns Observable story of the newly created saga
   */
  createSaga(title: string): Observable<HttpResponse<Story>> {
    const headers = this.buildHeader();
    console.log(headers);
    return this.http
      .post<Story>(
        environment.apiUrl + this.SAGAS, {title}, { headers, observe: 'response'})
        .pipe(catchError(this.errorHandler.handleError));
  }

  /**
   * @method GET
   * @param sagaId ID of the saga/story
   * @returns Raw JSON array of each branch for a particular Saga
   */
  getStory(sagaId: string): Observable<any> {
    const headers = this.buildHeader();
    console.log(headers);
    return this.http
      .get<any>(
        environment.apiUrl + this.BRANCHES + '/' + sagaId, {
          headers
        }
      ).pipe(catchError(this.errorHandler.handleError));
  }

  /**
   * @param id ID of the story to be emitted in the event
   */
  emitStory(id) {
    this.storyChange.emit(id);
  }

  /**
   * @returns EventEmitter with the saga id for observing component to subscribe to
   */
  getStoryEvent(): EventEmitter<number> {
    return this.storyChange;
  }

  private buildHeader(): HttpHeaders {
    if (this.user) {
      console.log('hit');
      return new HttpHeaders(
        {
          'X-Requested-With': 'XMLHttpRequest',
          Authorization: 'Bearer ' + this.token
        }
      );
    } else {
      return;
    }
  }
}
