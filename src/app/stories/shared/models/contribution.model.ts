import { Branch } from './branch.model';

export class Contribution {
    constructor(
        public id: number,
        public content: string,
        public sagaId: number = null,
        public parentId: number = null,
        public createdAt: Date = null,
        public associatedBranches: Branch[] = []
    ) { }
}
