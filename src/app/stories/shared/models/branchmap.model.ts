export class BranchMap {
  public parentTail: number;
  public head: number;
  public tail: number;
  public contributions: {id: number, contents: string}[];

  constructor(parentTail: number, head: number, tail: number, contributions: {id: number, contents: string}[] = []) {
    this.parentTail = parentTail;
    this.head = head;
    this.tail = tail;
    this.contributions = contributions;
  }
}
