import { Contribution } from './contribution.model';

export class Story {
    id?: number;
    public contributions: Contribution[] = [];
}
