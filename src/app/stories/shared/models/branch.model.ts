import { Contribution } from './contribution.model';

export class Branch {
    public parentId: number;
    public tailId: number;
    public contributions: Contribution[];

    constructor(parentId: number, tailId: number, contributions: Contribution[] = []) {
        this.parentId = parentId;
        this.tailId = tailId;
        this.contributions = contributions;
    }
}
