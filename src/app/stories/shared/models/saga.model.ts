export class Saga {
  constructor(
    public id: number,
    public title: string,
    public createdAt: Date
  ) { }
}
