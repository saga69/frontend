import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AddStoryComponent } from './story/add-story/add-story.component';
import { StoryService } from './shared/services/story.service';
import { Saga } from './shared/models/saga.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss']
})
export class StoriesComponent implements OnInit {

  sagas: Saga[] = [];

  constructor(
    private dialog: MatDialog,
    private storyService: StoryService,
    private router: Router
    ) {}

  ngOnInit(): void {
    this.storyService.emitStory(null);

    this.getAllSagas();
  }

  newStoryDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    this.dialog.open(AddStoryComponent, dialogConfig);

  }

  getAllSagas(): void {
    this.storyService.getSagas().subscribe(sagas => {
      this.sagas = sagas;
    });
  }

  onSelectSaga(saga: Saga) {
    this.router.navigate([`sagas/${saga.id}`]);
  }
}
