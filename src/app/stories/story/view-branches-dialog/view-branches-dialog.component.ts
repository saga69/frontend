import { Component, OnInit, Inject, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgbCarouselConfig, NgbSlideEvent } from '@ng-bootstrap/ng-bootstrap';
import { Branch } from '../../shared/models/branch.model';

@Component({
  selector: 'app-view-branches-dialog',
  templateUrl: './view-branches-dialog.component.html',
  styleUrls: ['./view-branches-dialog.component.scss'],
  providers: [NgbCarouselConfig]
})
export class ViewBranchesDialogComponent implements OnInit {

  @Output() emitContributionId: number;
  selectedBranch: Branch;
  isInitial = true;

  constructor(
    public dialogRef: MatDialogRef<ViewBranchesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    config: NgbCarouselConfig) {
      config.interval = 0;
    }

  ngOnInit(): void { }

  onSlide(event: NgbSlideEvent) {
    this.isInitial = event.current === 'initial';
    this.selectedBranch = this.isInitial ?
      null :
      this.data.contribution.associatedBranches[+event.current];
  }

  close() {
    this.dialogRef.close();
  }

  selectBranch() {
    this.dialogRef.close({branch: this.selectedBranch});
  }
}
