import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { FormControl, Validators, FormGroup, FormGroupDirective } from '@angular/forms';
import { StoryService } from '../../shared/services/story.service';
import { Contribution } from '../../shared/models/contribution.model';

@Component({
  selector: 'app-add-contribution',
  templateUrl: './add-contribution.component.html',
  styleUrls: ['./add-contribution.component.scss']
})
export class AddContributionComponent implements OnInit {

  @Input() lastContribution: Contribution;
  @Output() newContribution = new EventEmitter<string>();

  contributionForm = new FormGroup({
    contribution: new FormControl('', Validators.required)
  });

  constructor(private storyService: StoryService) { }

  ngOnInit() { }

  onAddContribution(formDirective: FormGroupDirective) {
    this.storyService.addContribution(
      this.lastContribution.sagaId,
      this.lastContribution.id,
      this.contributionForm.controls.contribution.value
    ).subscribe(response => {
      const id = response.body.id.toString();
      this.newContribution.emit(id);
    });
    formDirective.resetForm(); // allows field to reset validation state
    this.contributionForm.reset();
  }
}
