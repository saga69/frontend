import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-branch-dialog',
  templateUrl: './add-branch-dialog.component.html',
  styleUrls: ['./add-branch-dialog.component.scss']
})
export class AddBranchDialogComponent implements OnInit {

  addBranchForm = new FormGroup({
    contribution: new FormControl('', Validators.required)
  });

  constructor(
    public dialogRef: MatDialogRef<AddBranchDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() { }

  close() {
    this.dialogRef.close();
  }

  addBranch() {
    this.dialogRef.close({content: this.addBranchForm.controls.contribution.value});
  }
}
