import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { StoryService } from '../../shared/services/story.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-story',
  templateUrl: './add-story.component.html',
  styleUrls: ['./add-story.component.scss']
})
export class AddStoryComponent implements OnInit {

  createSagaForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<AddStoryComponent>,
              private storyService: StoryService,
              private router: Router) { }

  ngOnInit() {
    this.createSagaForm = this.formBuilder.group({
      title: ['', Validators.required],
      contribution: ['', Validators.required]
    });
  }

  cancel() {
    this.dialogRef.close();
  }

  createSaga() {
    this.dialogRef.close();

    let id: number;

    this.storyService.createSaga(
      this.createSagaForm.controls.title.value
      ).subscribe(response => {
        id = response.body.id;

        this.storyService.addContribution(
          id,
          null,
          this.createSagaForm.controls.contribution.value
          ).subscribe(() => {
            this.storyService.emitStory(id);
            this.router.navigateByUrl(`/sagas/${id}`);
          });

      });
  }
}
