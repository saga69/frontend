import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Contribution } from '../shared/models/contribution.model';
import { StoryService } from '../shared/services/story.service';
import { Story } from '../shared/models/story.model';
import { Branch } from '../shared/models/branch.model';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss']
})
export class StoryComponent implements OnInit {

  story: Story;
  contributions: Contribution[];
  lastContribution: Contribution;
  isLoading: boolean;
  private allBranches: Branch[];
  private storyId: string;

  constructor(
    private snackBar: MatSnackBar,
    private storyService: StoryService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.storyId = this.route.snapshot.paramMap.get('id');
    this.isLoading = true;
    this.allBranches = [];
    this.story = new Story();
    this.storyService.emitStory(parseInt(this.storyId, 10));
    this.startSocket();
    this.populateBranchData();
  }

  async startSocket() {

    // tslint:disable-next-line: max-line-length
    const ws = new WebSocket(`${environment.webSocketUrl}/sagas-${this.storyId}/${environment.javaWebtoken}`);

    ws.onerror = (e) => {
      console.log(e);
    };
    ws.onopen = (conn) => {
      console.log('web socket connection successful');
    };
    ws.onmessage = (message) => {
      this.openSnackBar();
    };

  }

  populateBranchData(): void {
    forkJoin(
      this.storyService.getStoryContributions(this.storyId),
      this.storyService.getStory(this.storyId)
    )
    .subscribe(([contributions, branches]) => {
      this.contributions = contributions;
      branches.forEach((branch: any) => {
        const newBranch = new Branch(branch.parent_tail, branch.tail);
        branch.contribution_ids.forEach((id: number, iterator: number) => {
          // find Contribution from current ID, add to branches array
          newBranch.contributions.push(
            this.contributions.find(contribution => {
              return contribution.id === id;
            })
          );
        });
        this.allBranches.push(newBranch);
      });
      this.generateStoryData();
    });
  }

  generateStoryData(): void {
    let currentTailId = null;
    let currentParentTailId = null;
    this.allBranches.forEach((branch: Branch, iterator: number) => {
      if (iterator === 0) {
        this.story.contributions.push(...branch.contributions);
        currentTailId = branch.tailId;
      } else if (branch.parentId === currentTailId) {
        branch.contributions.forEach(contribution => {
          this.story.contributions.push(contribution);
        });
        currentTailId = branch.tailId;
      } else {
        // duplicate branch parent id, add it to the existing contribution
        const index = this.story.contributions.findIndex(contribution => {
          return contribution.parentId === branch.parentId;
        });
        if (index >= 0) {
          this.story.contributions[index].associatedBranches.push(branch);
        }
      }
      currentParentTailId = branch.parentId;
    });
    this.updateLastContribution();
    this.isLoading = false;
  }

  openSnackBar() {
    const message = 'new contribution, refresh to be up to date';
    const action = 'Clear';
    this.snackBar.open(message, action, {
      duration: 3000
    });
  }

  onNewContribution(contributionId: string) {
    this.storyService.getContribution(contributionId)
      .subscribe(contribution => {
        let tempContr: Contribution;
        tempContr = {
          id: contribution.id,
          parentId: contribution.parent_id,
          sagaId: contribution.saga_id,
          createdAt: contribution.created_at,
          content: contribution.content,
          associatedBranches: []
        };
        this.story.contributions.push(tempContr);
      });
  }

  handleBranchChange(branchToAdd: Branch): void {
    // delete all story data starting at branch replacing
    this.story.contributions.splice(
      this.story.contributions.findIndex(contribution => {
        return contribution.parentId === branchToAdd.parentId;
    }));

    let tailId = null;
    do {
      branchToAdd.contributions.forEach(contribution => {
        this.allBranches.forEach(branch => {
          if (branch.parentId === contribution.parentId &&
              branchToAdd.tailId !== branch.tailId) {
            if (contribution.associatedBranches.indexOf(branch) === -1) {
              contribution.associatedBranches.push(branch);
            }
          }
        });
        this.story.contributions.push(contribution);
        tailId = contribution.id;
      });
      branchToAdd = this.allBranches.find(branch => {
        return branch.parentId === tailId;
      });
    } while (branchToAdd);
    this.updateLastContribution();
  }

  handleAddBranch(id: string) {
    console.log('hit');
    this.storyService.getContribution(id)
    .subscribe(contribution => {
      console.log(contribution);
      let tempContr: Contribution;
      tempContr = {
        id: contribution.id,
        parentId: contribution.parent_id,
        sagaId: contribution.saga_id,
        createdAt: contribution.created_at,
        content: contribution.content,
        associatedBranches: []
      };
      const indexToReplace = this.story.contributions.findIndex(storyContribution => {
        return storyContribution.parentId === tempContr.parentId;
      });
      this.story.contributions.splice(indexToReplace, this.story.contributions.length, tempContr);
    });
  }

  updateLastContribution(): void {
    this.lastContribution = this.story.contributions[this.story.contributions.length - 1];
  }
}
