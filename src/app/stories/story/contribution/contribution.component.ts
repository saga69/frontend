import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Contribution } from '../../shared/models/contribution.model';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ViewBranchesDialogComponent } from '../view-branches-dialog/view-branches-dialog.component';
import { Branch } from '../../shared/models/branch.model';
import { AddBranchDialogComponent } from '../add-branch-dialog/add-branch-dialog.component';
import { StoryService } from '../../shared/services/story.service';

@Component({
  selector: 'app-contribution',
  templateUrl: './contribution.component.html',
  styleUrls: ['./contribution.component.scss']
})
export class ContributionComponent implements OnInit {

  @Input() contribution: Contribution;
  @Input() isLastContribution: boolean;
  @Output() branchChange: EventEmitter<Branch> = new EventEmitter();
  @Output() addBranch: EventEmitter<string> = new EventEmitter();

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ViewBranchesDialogComponent>,
    public addBranchDialogRef: MatDialogRef<AddBranchDialogComponent>,
    private storyService: StoryService) {
    }

  ngOnInit() {
    console.log(this.contribution);
   }

  openDialog(): void {
    this.dialogRef = this.dialog.open(ViewBranchesDialogComponent, {
      data: {contribution: this.contribution}
    });
    this.dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.branchChange.emit(data.branch);
      }
    });
  }

  openAddBranchDialog(): void {
    this.addBranchDialogRef = this.dialog.open(AddBranchDialogComponent, {
      width: '85vw',
      data: {parentId: this.contribution.parentId}
    });
    this.addBranchDialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.storyService.addContribution(
          this.contribution.sagaId,
          this.contribution.parentId,
          data.content
        ).subscribe(response => {
          const id = response.body.id.toString();
          this.addBranch.emit(id);
        });
      }
    });
  }
}
