import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StoryService } from 'src/app/stories/shared/services/story.service';
import { MatDialogRef } from '@angular/material';
import { Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import { BranchMap } from 'src/app/stories/shared/models/branchmap.model';

@Component({
  selector: 'app-node-dialog',
  templateUrl: './node-dialog.component.html',
  styleUrls: ['./node-dialog.component.scss']
})
export class NodeDialogComponent implements OnInit {

  branch: BranchMap;
  title: string;

  constructor(
    private dialogRef: MatDialogRef<NodeDialogComponent>,
    private storyService: StoryService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public branchData: any
    ) {}

  ngOnInit() {
    this.branch = this.branchData.dataKey;
    this.title = `Branch #${this.branch.head}`;
    console.log(this.branch);
  }

  navToBranch() {

  }

  close() {
    this.dialogRef.close();
  }
}
