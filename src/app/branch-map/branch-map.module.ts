import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../modules/material.module';
import { NodeDialogComponent } from './node-dialog/node-dialog.component';
import { BootstrapModule } from '../modules/bootstrap.module';
import { BranchMapComponent } from './branch-map.component';

@NgModule({
  declarations: [NodeDialogComponent, BranchMapComponent],
  imports: [
    CommonModule,
    MaterialModule,
    BootstrapModule
  ],
  entryComponents: [
    NodeDialogComponent
  ]
})
export class BranchMapModule { }
