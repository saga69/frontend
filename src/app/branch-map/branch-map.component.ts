import { Component, OnInit } from '@angular/core';
import sigma from 'sigma';
import { ActivatedRoute } from '@angular/router';
import { StoryService } from '../stories/shared/services/story.service';
import { BranchMap } from '../stories/shared/models/branchmap.model';
import { MatDialog } from '@angular/material';
import { NodeDialogComponent } from './node-dialog/node-dialog.component';

@Component({
  selector: 'app-branch-map',
  templateUrl: './branch-map.component.html',
  styleUrls: ['./branch-map.component.scss']
})
export class BranchMapComponent implements OnInit {

  private storyId: string;
  private branches: BranchMap[] = [];

  private sigma: any;

  private graph = {
    nodes: [],
    edges: []
  };

  constructor(private storyService: StoryService, private route: ActivatedRoute, private dialog: MatDialog) {}

  ngOnInit() {
    this.storyId = this.route.snapshot.paramMap.get('id');
    this.storyService.emitStory(parseInt(this.storyId, 10));

    this.sigma = new sigma(
      {
        renderer: {
          container: document.getElementById('sigma-container'),
          type: 'canvas'
        },
        settings: {}
      }
    );
    this.sigma.cameras[0].goTo({ x: 350, y: 0, angle: 0, ratio: 2 });

    this.sigma.bind('clickNode', (e) => {
      const id = e.data.node.id;
      const tempBranch = this.branches.find( (br) => {
        return br.tail === id;
      });

      this.dialog.open(NodeDialogComponent, {
        autoFocus: true,
        data: {
          dataKey: tempBranch
        }
      });
    });

    this.getBranchData();
  }

  getBranchData() {
    this.storyService.getStory(this.storyId)
    .subscribe(branches => {
      for (const branch of branches) {
        this.parseBranch(branch);
      }
      this.fractalRender();
    });
  }

  parseBranch(branch: any) {
    const tempArray: string[] = branch.contribution_contents;
    const tempBranch = new BranchMap(branch.parent_tail, branch.head, branch.tail);

    tempArray.forEach((a, i) => {
      tempBranch.contributions.push({id: branch.contribution_ids[i], contents: a});
    });

    this.branches.push(tempBranch);
  }

  fractalRender() {
    let i = 0;
    // tslint:disable-next-line: interface-over-type-literal
    type branch = {
      pt: number | null,
      tail: number,
      x: number | null,
      y: number | null,
      angle: number | null ,
      visibleAngle: number | null,
      label: string,
      size: number
    };

    const mappedbranches: branch[] = this.branches.map(br => {
      return {
        pt: br.parentTail,
        tail: br.tail,
        x: null,
        y: null,
        angle: 100,
        visibleAngle: 360,
        label: br.contributions[0].contents,
        size: br.contributions.length
      };
    });
    let curlayer: branch[] = [mappedbranches[0]];
    let nextlayer: Map<branch, branch[]> = new Map();
    let curRad = 0.0;
    let x = 10;
    this.graph.nodes.push({
      id: mappedbranches[0].tail,
      label: mappedbranches[0].label,
      x: 0,
      y: 0,
      size: mappedbranches[0].size,
      color: '#008cc2'}
    );

    while (true) {
      const realNextLayer: branch[] = [];
      nextlayer = new Map();
      curRad += x;
      x *= 0.5;

      curlayer.forEach(cb => {
        nextlayer.set(cb, mappedbranches.filter(b => b.pt === cb.tail));
      });
      if (nextlayer.size === 0) { break; }

      curlayer.forEach(cb => {
        // tslint:disable-next-line: max-line-length
        const curAngleStep = (cb.visibleAngle || 0) / ((nextlayer.get(cb) || []).length + ((cb.visibleAngle || 0) === 360 ? 0 : 1));
        const newVisibilityAngle = (cb.visibleAngle || 0) / (nextlayer.get(cb) || []).length;
        let startAngle = (cb.angle || 0) - (cb.visibleAngle || 0) / 2;

        (nextlayer.get(cb) || []).forEach(nb => {
          startAngle += curAngleStep;
          // tslint:disable-next-line: no-shadowed-variable
          const x = Math.cos(startAngle * 3.14 / 180.0) * curRad;
          const y = Math.sin(startAngle * 3.14 / 180.0) * curRad;
          realNextLayer.push({
            pt: nb.pt,
            tail: nb.tail,
            // tslint:disable-next-line: object-literal-shorthand
            x: x, y: y,
            angle: startAngle, visibleAngle: newVisibilityAngle,
            label: nb.label,
            size: nb.size
          });
          // tslint:disable-next-line: object-literal-shorthand
          this.graph.nodes.push({id: nb.tail, label: nb.label, x: x, y: y, size: nb.size, color: '#008cc2'});
          // tslint:disable-next-line: max-line-length
          this.graph.edges.push({id: `e${i}`, source: nb.tail, target: cb.tail, color: '#008cc2', type: 'curve', size: 1});
          i += 1;
        });
      });
      curlayer = realNextLayer;
    }

    this.sigma.graph.read(this.graph);
    this.sigma.refresh();
  }
}
