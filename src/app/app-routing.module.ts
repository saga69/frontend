import { StoryComponent } from './stories/story/story.component';
import { ContributionComponent } from './stories/story/contribution/contribution.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoriesComponent } from './stories/stories.component';
import { BranchMapComponent } from './branch-map/branch-map.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', component: StoriesComponent, canActivate: [AuthGuard]},
  { path: 'login', component: LoginComponent },
  { path: 'sagas/:id', component: StoryComponent, canActivate: [AuthGuard] },
  { path: 'contributions/:id', component: ContributionComponent, canActivate: [AuthGuard] },
  { path: 'register', component: RegisterComponent },
  { path: 'branchmap/:id', component: BranchMapComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
