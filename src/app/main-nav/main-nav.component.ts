import { Component, OnInit, OnDestroy } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { StoryService } from '../stories/shared/services/story.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent implements OnInit, OnDestroy {

  storyId$: Subscription;
  storyId: number;
  userSub: Subscription;
  user: firebase.User;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
      private breakpointObserver: BreakpointObserver,
      private storyService: StoryService,
      private router: Router,
      private afAuth: AngularFireAuth
    ) {
      this.userSub = this.afAuth.user.subscribe(user => {
        console.log(user);
        if (user) {
          this.user = user;
        } else {
          this.user = null;
        }
      });
    }

  ngOnInit(): void {
    this.storyId$ = this.storyService.getStoryEvent().subscribe(
      storyEventId => this.storyId = storyEventId
    );
  }

  ngOnDestroy(): void {
    this.storyId$.unsubscribe();
    this.userSub.unsubscribe();
  }

  navToBranchMap() {
    this.router.navigateByUrl(`/branchmap/${this.storyId}`);
  }

  navToSaga() {
    this.router.navigateByUrl(`/sagas/${this.storyId}`);
  }

  navToLogin() {
    this.router.navigate(['/login']);
  }

  navToRegister() {
    this.router.navigate(['/register']);
  }

  async logout() {
    await this.afAuth.auth.signOut();
    this.router.navigate(['/login']);
  }
}
