import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EMAIL_PATTERN } from '../shared/regex';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  error: string;

  constructor(private fb: FormBuilder, private router: Router, private afAuth: AngularFireAuth) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(EMAIL_PATTERN)]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  ngOnInit() {
    this.afAuth.user.subscribe(user => {
      if (user) {
        this.afAuth.auth.currentUser.getIdToken().then(token => {
          console.log(token);
        });
      }
    });
  }

  async login() {
    const user = this.loginForm.value;
    await this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);
    this.router.navigate(['']);
  }

  navToRegister() {
    this.router.navigate(['/register']);
  }
}
