import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EMAIL_PATTERN } from '../shared/regex';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { MustMatch } from '../helpers/must-match.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;
  error: string;

  constructor(private fb: FormBuilder, private router: Router, private afAuth: AngularFireAuth) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      displayName: ['', [Validators.required]],
      emailConfirm: ['', [Validators.required]],
      passwordConfirm: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern(EMAIL_PATTERN)]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    },
    {
      validators: [
        MustMatch('email', 'emailConfirm'),
        MustMatch('password', 'passwordConfirm')
      ]
    });
  }

  async register() {
    const user = this.registerForm.value;
    await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);

    const newUser = this.afAuth.auth.currentUser;
    await newUser.updateProfile({
      displayName: user.displayName
    });

    this.router.navigate(['']);
  }

  navToLogin() {
    this.router.navigate(['/login']);
  }
}
